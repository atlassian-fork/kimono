//
//  KMSmartbar.h
//  Kimono
//
//  Created by James Dumay on 29/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "KMPageUpdater.h"

@interface KMSmartbar : NSTextField <KMPageUpdater>


@property (assign, readwrite) double currentProgress;

@end
