//
//  KMBrowserWindowView.h
//  Kimono
//
//  Created by James Dumay on 27/08/12.
//  Copyright (c) 2012 Whimsy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "KMBrowserToolbar.h"

@interface KMBrowserWindowView : NSView

@property (strong) NSTrackingArea *toolbarArea;

-(KMBrowserToolbar*)toolbar;
-(NSView*)innerView;

@end
