//
//  KMWebView.h
//  Kimono
//
//  Created by James Dumay on 12/04/12.
//  Copyright (c) 2012 Whimsy. All rights reserved.
//

#import <WebKit/WebKit.h>

@interface KMWebView : WebView

@end
