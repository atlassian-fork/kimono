//
//  KMAppDelegate.m
//  Kimono
//
//  Created by James Dumay on 23/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMAppDelegate.h"
#import "KMBrowserWindowController.h"

@implementation KMAppDelegate

@synthesize controllers = _controllers;

KMBrowserWindowController *controller;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    _controllers = [[NSMutableArray alloc] init];
    [self createNewWindow:[[NSURL alloc] initWithString:@"http://www.smh.com.au"]];
}

- (IBAction)openNewWindow:(id)sender
{
    NSWindowController *controller = [self createNewWindow:nil];
    [controller.window makeKeyAndOrderFront:self];
}

- (IBAction)openNewTab:(id)sender 
{
    KMBrowserWindowController *window = [self currentBrowserWindow];
    [window closePopovers];
    [window createNewTab:nil];
}

- (IBAction)focusSmartbar:(id)sender 
{
    [[self currentBrowserWindow] focusSmartbar];
}

- (IBAction)stopLoad:(id)sender 
{
    [[self currentBrowserWindow] stopLoad];
}

- (IBAction)reload:(id)sender 
{
    [[self currentBrowserWindow] reload];
}

- (IBAction)showTabs:(id)sender 
{
    [[self currentBrowserWindow] showTabs];
}

- (IBAction)closeTab:(id)sender 
{
    KMBrowserWindowController *controller = [self currentBrowserWindow];
    if ([controller currentTab] != nil)
    {
        [controller closeCurrentTab];
    }
    else
    {
        [controller close];
    }
}

- (NSWindowController*)createNewWindow:(NSURL*)url
{
    KMBrowserWindowController *controller = [[KMBrowserWindowController alloc] init];
    [_controllers addObject:controller];
    [[controller window] makeKeyAndOrderFront:self];
    
    [controller createNewTab:url];
    return controller;
}

- (KMBrowserWindowController *)currentBrowserWindow
{
    NSWindow *window = [NSApp mainWindow];
    return (KMBrowserWindowController*)[window windowController];
}

@end
