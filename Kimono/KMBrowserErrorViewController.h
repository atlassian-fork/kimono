//
//  KMBrowserErrorViewController.h
//  Kimono
//
//  Created by James Dumay on 7/04/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KMBrowserErrorViewController : NSViewController

@property (strong) IBOutlet NSTextField *errorLabel;

- (void)urlCouldNotBeLoaded:(NSURL*)url withMessage:(NSString*)message;

@end
