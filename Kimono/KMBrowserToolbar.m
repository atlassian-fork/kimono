//
//  KMBrowserToolbar.m
//  Kimono
//
//  Created by James Dumay on 28/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMBrowserToolbar.h"

#import "KMBrowserWindowController.h"

@implementation KMBrowserToolbar

@synthesize timer = _timer;

-(NSView*)toolbar
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller toolbar];
}

-(NSView *)innerView;
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller innerView];
}

-(NSView *)view
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller view];
}

-(NSTextField*)smartBar
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller smartBar];
}

-(NSPopover*)tabsPopover
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller tabsPopover];
}

-(void)showWithDelay:(NSView*)view;
{
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:view forKey:@"view"];
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(showForTimer:) userInfo:userInfo repeats:NO];
}

-(void)show:(NSView*)view
{
    NSRect toolbarFrame = NSMakeRect(view.frame.origin.x, view.frame.size.height - 36, view.frame.size.width, 36);
    
    [self setFrame:toolbarFrame];
    [[view animator] addSubview:self];
}

-(void)showForTimer:(NSTimer*)timer
{
    NSDictionary *userInfo = [timer userInfo];
    NSView *view = [userInfo objectForKey:@"view"];
    [self show:view];
}

-(void)hideToolbar:(NSTimer*)timer
{
    [self hide];
}

-(void)hide
{
    [_timer invalidate];
    
    //    NSWindow *currentWindow = [[self view] window];
    if (![[self tabsPopover] isShown]) // && !([[self smartBar] isEqualTo:[currentWindow firstResponder]]))
    {
        [[[self toolbar] animator] removeFromSuperview];
    }
    else
    {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(hideToolbar:) userInfo:nil repeats:NO];
    }
}

- (void)drawRect:(NSRect)rect
{
    [[NSColor windowFrameColor] set];
    NSRectFill([self bounds]);
    
    NSBezierPath *bezierPath = [NSBezierPath bezierPath];
    [bezierPath moveToPoint:NSMakePoint(0, 0)];
    [bezierPath lineToPoint:NSMakePoint(rect.size.width, 0)];
    [bezierPath closePath];
    [bezierPath setLineWidth: 1];
    [[NSColor colorWithCalibratedRed:0.50 green:0.50 blue:0.50 alpha:1.00] set];
//    [[NSColor controlShadowColor] set];
    [bezierPath stroke];
}

@end
